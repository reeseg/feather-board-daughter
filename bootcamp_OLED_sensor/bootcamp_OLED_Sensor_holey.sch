<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="4" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="1" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="1" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="1" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="1" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="1" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="4" fill="1" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="0" fill="1" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="0" fill="1" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Molex - 54104-3033">
<description>Upverter Parts Library

Created by Upverter.com</description>
<packages>
<package name="MOLEX_54104-3033_0">
<description>MOLX-54104-3033</description>
<polygon width="0" layer="1">
<vertex x="-8.15" y="-2.55"/>
<vertex x="-8.15" y="-0.15"/>
<vertex x="-9.75" y="-0.15"/>
<vertex x="-9.75" y="-0.95"/>
<vertex x="-10.55" y="-0.95"/>
<vertex x="-10.55" y="-2.55"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-8.15" y="-2.55"/>
<vertex x="-8.15" y="-0.15"/>
<vertex x="-9.75" y="-0.15"/>
<vertex x="-9.75" y="-0.95"/>
<vertex x="-10.55" y="-0.95"/>
<vertex x="-10.55" y="-2.55"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-8.15" y="-2.55"/>
<vertex x="-8.15" y="-0.15"/>
<vertex x="-9.75" y="-0.15"/>
<vertex x="-9.75" y="-0.95"/>
<vertex x="-10.55" y="-0.95"/>
<vertex x="-10.55" y="-2.55"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="8.15" y="-2.55"/>
<vertex x="8.15" y="-0.15"/>
<vertex x="9.75" y="-0.15"/>
<vertex x="9.75" y="-0.95"/>
<vertex x="10.55" y="-0.95"/>
<vertex x="10.55" y="-2.55"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="8.15" y="-2.55"/>
<vertex x="8.15" y="-0.15"/>
<vertex x="9.75" y="-0.15"/>
<vertex x="9.75" y="-0.95"/>
<vertex x="10.55" y="-0.95"/>
<vertex x="10.55" y="-2.55"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="8.15" y="-2.55"/>
<vertex x="8.15" y="-0.15"/>
<vertex x="9.75" y="-0.15"/>
<vertex x="9.75" y="-0.95"/>
<vertex x="10.55" y="-0.95"/>
<vertex x="10.55" y="-2.55"/>
</polygon>
<wire x1="-10.25" y1="-5.5" x2="10.25" y2="-5.5" width="0.1" layer="51"/>
<wire x1="10.25" y1="-5.5" x2="10.25" y2="-4.65" width="0.1" layer="51"/>
<wire x1="9.65" y1="-4.65" x2="10.25" y2="-4.65" width="0.1" layer="51"/>
<wire x1="9.65" y1="-4.65" x2="9.65" y2="1.05" width="0.1" layer="51"/>
<wire x1="-9.65" y1="-4.65" x2="-9.65" y2="1.05" width="0.1" layer="51"/>
<wire x1="-10.25" y1="-4.65" x2="-9.65" y2="-4.65" width="0.1" layer="51"/>
<wire x1="-9.65" y1="1.05" x2="9.65" y2="1.05" width="0.1" layer="51"/>
<wire x1="-10.25" y1="-5.5" x2="-10.25" y2="-4.65" width="0.1" layer="51"/>
<wire x1="-10.25" y1="-5.493" x2="-10.25" y2="-5.493" width="0.1" layer="51"/>
<wire x1="-9.65" y1="1.05" x2="-7.8" y2="1.05" width="0.15" layer="21"/>
<wire x1="-9.65" y1="-4.65" x2="-9.65" y2="-2.95" width="0.15" layer="21"/>
<wire x1="9.65" y1="-4.65" x2="9.65" y2="-2.95" width="0.15" layer="21"/>
<wire x1="9.65" y1="-4.65" x2="10.25" y2="-4.65" width="0.15" layer="21"/>
<wire x1="-10.25" y1="-5.5" x2="-10.25" y2="-4.65" width="0.15" layer="21"/>
<wire x1="-10.25" y1="-4.65" x2="-9.65" y2="-4.65" width="0.15" layer="21"/>
<wire x1="-9.65" y1="0.25" x2="-9.65" y2="1.05" width="0.15" layer="21"/>
<wire x1="9.65" y1="0.25" x2="9.65" y2="1.05" width="0.15" layer="21"/>
<wire x1="7.8" y1="1.05" x2="9.65" y2="1.05" width="0.15" layer="21"/>
<wire x1="10.25" y1="-5.5" x2="10.25" y2="-4.65" width="0.15" layer="21"/>
<wire x1="-10.25" y1="-5.5" x2="10.25" y2="-5.5" width="0.15" layer="21"/>
<wire x1="-10.65" y1="-5.6" x2="-10.65" y2="2.05" width="0.1" layer="39"/>
<wire x1="-10.65" y1="2.05" x2="10.65" y2="2.05" width="0.1" layer="39"/>
<wire x1="10.65" y1="2.05" x2="10.65" y2="-5.6" width="0.1" layer="39"/>
<wire x1="10.65" y1="-5.6" x2="-10.65" y2="-5.6" width="0.1" layer="39"/>
<text x="-11.65" y="2.625" size="1" layer="25">&gt;NAME</text>
<circle x="-7.25" y="2.5" radius="0.125" width="0.25" layer="21"/>
<smd name="1" x="-7.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="2" x="-6.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="4" x="-5.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="3" x="-6.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="7" x="-4.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="8" x="-3.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="6" x="-4.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="5" x="-5.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="13" x="-1.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="14" x="-0.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="16" x="0.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="15" x="-0.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="11" x="-2.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="12" x="-1.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="10" x="-2.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="9" x="-3.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="25" x="4.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="26" x="5.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="28" x="6.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="27" x="5.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="30" x="7.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="29" x="6.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="21" x="2.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="22" x="3.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="24" x="4.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="23" x="3.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="19" x="1.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="20" x="2.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="18" x="1.25" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="17" x="0.75" y="1.35" dx="0.3" dy="1.2" layer="1"/>
<smd name="MP1" x="-8.95" y="-1.35" dx="1" dy="1" layer="1"/>
<smd name="MP2" x="8.95" y="-1.35" dx="1" dy="1" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="MOLEX_54104-3033_0_0">
<description>MOLX-54104-3033</description>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-8.382" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="25.4" y2="-8.382" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="-8.382" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="-8.382" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="-8.382" width="0.254" layer="94"/>
<wire x1="48.26" y1="-10.16" x2="48.26" y2="-8.382" width="0.254" layer="94"/>
<wire x1="45.72" y1="-10.16" x2="45.72" y2="-8.382" width="0.254" layer="94"/>
<wire x1="43.18" y1="-10.16" x2="43.18" y2="-8.382" width="0.254" layer="94"/>
<wire x1="40.64" y1="-10.16" x2="40.64" y2="-8.382" width="0.254" layer="94"/>
<wire x1="38.1" y1="-10.16" x2="38.1" y2="-8.382" width="0.254" layer="94"/>
<wire x1="35.56" y1="-10.16" x2="35.56" y2="-8.382" width="0.254" layer="94"/>
<wire x1="33.02" y1="-10.16" x2="33.02" y2="-8.382" width="0.254" layer="94"/>
<wire x1="30.48" y1="-10.16" x2="30.48" y2="-8.382" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="-8.382" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="-8.382" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="-8.382" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="-8.382" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-8.382" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.254" layer="94"/>
<wire x1="58.42" y1="-10.16" x2="58.42" y2="-8.382" width="0.254" layer="94"/>
<wire x1="55.88" y1="-10.16" x2="55.88" y2="-8.382" width="0.254" layer="94"/>
<wire x1="53.34" y1="-10.16" x2="53.34" y2="-8.382" width="0.254" layer="94"/>
<wire x1="50.8" y1="-10.16" x2="50.8" y2="-8.382" width="0.254" layer="94"/>
<wire x1="76.2" y1="-10.16" x2="76.2" y2="-8.382" width="0.254" layer="94"/>
<wire x1="73.66" y1="-10.16" x2="73.66" y2="-8.382" width="0.254" layer="94"/>
<wire x1="71.12" y1="-10.16" x2="71.12" y2="-8.382" width="0.254" layer="94"/>
<wire x1="68.58" y1="-10.16" x2="68.58" y2="-8.382" width="0.254" layer="94"/>
<wire x1="66.04" y1="-10.16" x2="66.04" y2="-8.382" width="0.254" layer="94"/>
<wire x1="63.5" y1="-10.16" x2="63.5" y2="-8.382" width="0.254" layer="94"/>
<wire x1="60.96" y1="-10.16" x2="60.96" y2="-8.382" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="86.36" y2="-5.08" width="0.254" layer="94"/>
<wire x1="86.36" y1="-5.08" x2="86.36" y2="-10.16" width="0.254" layer="94"/>
<wire x1="86.36" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-10.16" width="0.15" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-10.16" width="0.15" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.15" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.15" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="-10.16" width="0.15" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="-10.16" width="0.15" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="-10.16" width="0.15" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="-10.16" width="0.15" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="-10.16" width="0.15" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="25.4" y2="-10.16" width="0.15" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-10.16" width="0.15" layer="94"/>
<wire x1="30.48" y1="-10.16" x2="30.48" y2="-10.16" width="0.15" layer="94"/>
<wire x1="33.02" y1="-10.16" x2="33.02" y2="-10.16" width="0.15" layer="94"/>
<wire x1="35.56" y1="-10.16" x2="35.56" y2="-10.16" width="0.15" layer="94"/>
<wire x1="38.1" y1="-10.16" x2="38.1" y2="-10.16" width="0.15" layer="94"/>
<wire x1="40.64" y1="-10.16" x2="40.64" y2="-10.16" width="0.15" layer="94"/>
<wire x1="43.18" y1="-10.16" x2="43.18" y2="-10.16" width="0.15" layer="94"/>
<wire x1="45.72" y1="-10.16" x2="45.72" y2="-10.16" width="0.15" layer="94"/>
<wire x1="48.26" y1="-10.16" x2="48.26" y2="-10.16" width="0.15" layer="94"/>
<wire x1="81.28" y1="-10.16" x2="81.28" y2="-10.16" width="0.15" layer="94"/>
<wire x1="83.82" y1="-10.16" x2="83.82" y2="-10.16" width="0.15" layer="94"/>
<wire x1="50.8" y1="-10.16" x2="50.8" y2="-10.16" width="0.15" layer="94"/>
<wire x1="53.34" y1="-10.16" x2="53.34" y2="-10.16" width="0.15" layer="94"/>
<wire x1="55.88" y1="-10.16" x2="55.88" y2="-10.16" width="0.15" layer="94"/>
<wire x1="58.42" y1="-10.16" x2="58.42" y2="-10.16" width="0.15" layer="94"/>
<wire x1="60.96" y1="-10.16" x2="60.96" y2="-10.16" width="0.15" layer="94"/>
<wire x1="63.5" y1="-10.16" x2="63.5" y2="-10.16" width="0.15" layer="94"/>
<wire x1="66.04" y1="-10.16" x2="66.04" y2="-10.16" width="0.15" layer="94"/>
<wire x1="68.58" y1="-10.16" x2="68.58" y2="-10.16" width="0.15" layer="94"/>
<wire x1="71.12" y1="-10.16" x2="71.12" y2="-10.16" width="0.15" layer="94"/>
<wire x1="73.66" y1="-10.16" x2="73.66" y2="-10.16" width="0.15" layer="94"/>
<wire x1="76.2" y1="-10.16" x2="76.2" y2="-10.16" width="0.15" layer="94"/>
<text x="0" y="-2.54" size="2.54" layer="95" align="top-left">&gt;NAME</text>
<text x="0" y="-15.24" size="2.54" layer="95" align="top-left">54104-3033</text>
<pin name="1" x="2.54" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="2" x="5.08" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="4" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="5" x="12.7" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="6" x="15.24" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="7" x="17.78" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="8" x="20.32" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="9" x="22.86" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="10" x="25.4" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="11" x="27.94" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="12" x="30.48" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="13" x="33.02" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="14" x="35.56" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="15" x="38.1" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="16" x="40.64" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="17" x="43.18" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="18" x="45.72" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="19" x="48.26" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="MP1" x="81.28" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="MP2" x="83.82" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="20" x="50.8" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="21" x="53.34" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="22" x="55.88" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="23" x="58.42" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="24" x="60.96" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="25" x="63.5" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="26" x="66.04" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="27" x="68.58" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="28" x="71.12" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="29" x="73.66" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<pin name="30" x="76.2" y="-15.24" visible="pad" length="middle" direction="pas" rot="R90"/>
<circle x="27.94" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="25.4" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="22.86" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="20.32" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="17.78" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="48.26" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="45.72" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="43.18" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="40.64" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="38.1" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="35.56" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="33.02" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="30.48" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="15.24" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="12.7" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="10.16" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="7.62" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="5.08" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="2.54" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="58.42" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="55.88" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="53.34" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="50.8" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="76.2" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="73.66" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="71.12" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="68.58" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="66.04" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="63.5" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="60.96" y="-7.62" radius="0.762" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOLEX_54104-3033" prefix="J">
<description>MOLX-54104-3033</description>
<gates>
<gate name="G$0" symbol="MOLEX_54104-3033_0_0" x="0" y="0"/>
</gates>
<devices>
<device name="MOLEX_54104-3033_0_0" package="MOLEX_54104-3033_0">
<connects>
<connect gate="G$0" pin="1" pad="1"/>
<connect gate="G$0" pin="10" pad="10"/>
<connect gate="G$0" pin="11" pad="11"/>
<connect gate="G$0" pin="12" pad="12"/>
<connect gate="G$0" pin="13" pad="13"/>
<connect gate="G$0" pin="14" pad="14"/>
<connect gate="G$0" pin="15" pad="15"/>
<connect gate="G$0" pin="16" pad="16"/>
<connect gate="G$0" pin="17" pad="17"/>
<connect gate="G$0" pin="18" pad="18"/>
<connect gate="G$0" pin="19" pad="19"/>
<connect gate="G$0" pin="2" pad="2"/>
<connect gate="G$0" pin="20" pad="20"/>
<connect gate="G$0" pin="21" pad="21"/>
<connect gate="G$0" pin="22" pad="22"/>
<connect gate="G$0" pin="23" pad="23"/>
<connect gate="G$0" pin="24" pad="24"/>
<connect gate="G$0" pin="25" pad="25"/>
<connect gate="G$0" pin="26" pad="26"/>
<connect gate="G$0" pin="27" pad="27"/>
<connect gate="G$0" pin="28" pad="28"/>
<connect gate="G$0" pin="29" pad="29"/>
<connect gate="G$0" pin="3" pad="3"/>
<connect gate="G$0" pin="30" pad="30"/>
<connect gate="G$0" pin="4" pad="4"/>
<connect gate="G$0" pin="5" pad="5"/>
<connect gate="G$0" pin="6" pad="6"/>
<connect gate="G$0" pin="7" pad="7"/>
<connect gate="G$0" pin="8" pad="8"/>
<connect gate="G$0" pin="9" pad="9"/>
<connect gate="G$0" pin="MP1" pad="MP1"/>
<connect gate="G$0" pin="MP2" pad="MP2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CIIVA_IDS" value="13017181"/>
<attribute name="CIRCUITS_LOADED" value="30"/>
<attribute name="COMPONENT_LINK_1_DESCRIPTION" value="Manufacturer URL"/>
<attribute name="COMPONENT_LINK_1_URL" value="http://www.molex.com/molex/index.jsp"/>
<attribute name="COMPONENT_LINK_3_DESCRIPTION" value="Package Specification"/>
<attribute name="COMPONENT_LINK_3_URL" value="http://www.molex.com/pdm_docs/sd/541043033_sd.pdf"/>
<attribute name="CONTACT_POSITION" value="Top"/>
<attribute name="CURRENT_MAX_PER_CONTACT" value="0.5A"/>
<attribute name="DATASHEET" value="http://www.molex.com/webdocs/datasheets/pdf/en-us/0541043033_FFC_FPC_CONNECTORS.pdf"/>
<attribute name="DURABILITY_MATING_CYCLES_MAX" value="20"/>
<attribute name="ENTRY_ANGLE" value="90degrees Angle"/>
<attribute name="FOOTPRINT_VARIANT_NAME_0" value="Manufacturer Recommended"/>
<attribute name="IMPORTED" value="yes"/>
<attribute name="IMPORTED_FROM" value="vault"/>
<attribute name="IMPORT_TS" value="1521847092"/>
<attribute name="MATED_HEIGHT" value="2.00mm"/>
<attribute name="MATERIAL___METAL" value="Phosphor Bronze"/>
<attribute name="MATERIAL___PLATING_MATING" value="Silver, Tin-Bismuth"/>
<attribute name="MATERIAL___PLATING_TERMINATION" value="Silver, Tin-Bismuth"/>
<attribute name="MF" value="Molex"/>
<attribute name="MOUNTING_TECHNOLOGY" value="Surface Mount"/>
<attribute name="MPN" value="54104-3033"/>
<attribute name="NUMBER_OF_ROWS" value="1"/>
<attribute name="ORIENTATION" value="Right Angle"/>
<attribute name="PACKAGE" value="54104-3033"/>
<attribute name="PACKAGE_DESCRIPTION" value="30-Lead FPC Connector, Pitch 0.5 mm"/>
<attribute name="PACKAGE_VERSION" value="Rev. C, 04/2014"/>
<attribute name="PACKING" value="Tape and Reel"/>
<attribute name="PCB_LOCATOR" value="No"/>
<attribute name="PCB_RETENTION" value="None"/>
<attribute name="PITCH___MATING_INTERFACE" value="0.50mm"/>
<attribute name="PITCH___TERMINATION_INTERFACE" value="0.50mm"/>
<attribute name="POLARIZED_TO_PCB" value="Yes"/>
<attribute name="PREFIX" value="J"/>
<attribute name="RELEASE_DATE" value="1411387099"/>
<attribute name="ROHS" value="Yes"/>
<attribute name="STACKABLE" value="No"/>
<attribute name="VAULT_GUID" value="596CBB84-DF13-43DA-99C5-F398D0841A4D"/>
<attribute name="VAULT_REVISION" value="CDE8D844-DC83-4B89-AE5A-4AA1280BEFCD"/>
<attribute name="VOLTAGE_MAX" value="50V"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="bootcamp">
<packages>
<package name="FEATHER-1X16">
<pad name="1" x="0" y="0" drill="1.0414"/>
<pad name="2" x="2.54" y="0" drill="1.0414"/>
<pad name="3" x="5.08" y="0" drill="1.0414"/>
<pad name="4" x="7.62" y="0" drill="1.0414"/>
<pad name="5" x="10.16" y="0" drill="1.0414"/>
<pad name="6" x="12.7" y="0" drill="1.0414"/>
<pad name="7" x="15.24" y="0" drill="1.0414"/>
<pad name="8" x="17.78" y="0" drill="1.0414"/>
<pad name="9" x="20.32" y="0" drill="1.0414"/>
<pad name="10" x="22.86" y="0" drill="1.0414"/>
<pad name="11" x="25.4" y="0" drill="1.0414"/>
<pad name="12" x="27.94" y="0" drill="1.0414"/>
<pad name="13" x="30.48" y="0" drill="1.0414"/>
<pad name="14" x="33.02" y="0" drill="1.0414"/>
<pad name="15" x="35.56" y="0" drill="1.0414"/>
<pad name="16" x="38.1" y="0" drill="1.0414"/>
</package>
<package name="FEATHER-1X12">
<pad name="1" x="0" y="0" drill="1.0414"/>
<pad name="2" x="2.54" y="0" drill="1.0414"/>
<pad name="3" x="5.08" y="0" drill="1.0414"/>
<pad name="4" x="7.62" y="0" drill="1.0414"/>
<pad name="5" x="10.16" y="0" drill="1.0414"/>
<pad name="6" x="12.7" y="0" drill="1.0414"/>
<pad name="7" x="15.24" y="0" drill="1.0414"/>
<pad name="8" x="17.78" y="0" drill="1.0414"/>
<pad name="9" x="20.32" y="0" drill="1.0414"/>
<pad name="10" x="22.86" y="0" drill="1.0414"/>
<pad name="11" x="25.4" y="0" drill="1.0414"/>
<pad name="12" x="27.94" y="0" drill="1.0414"/>
</package>
</packages>
<symbols>
<symbol name="PINHD16">
<wire x1="-6.35" y1="-22.86" x2="1.27" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-22.86" x2="1.27" y2="20.32" width="0.4064" layer="94"/>
<wire x1="1.27" y1="20.32" x2="-6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="-22.86" width="0.4064" layer="94"/>
<text x="-6.35" y="20.955" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="13" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="15" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD12">
<wire x1="-6.35" y1="-12.7" x2="1.27" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="1.27" y2="20.32" width="0.4064" layer="94"/>
<wire x1="1.27" y1="20.32" x2="-6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="20.32" x2="-6.35" y2="-12.7" width="0.4064" layer="94"/>
<text x="-6.35" y="20.955" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FEATHER-1X16">
<gates>
<gate name="G$1" symbol="PINHD16" x="66.04" y="-12.7"/>
</gates>
<devices>
<device name="" package="FEATHER-1X16">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FEATHER-1X12">
<gates>
<gate name="G$1" symbol="PINHD12" x="7.62" y="12.7"/>
</gates>
<devices>
<device name="" package="FEATHER-1X12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="Molex - 54104-3033" deviceset="MOLEX_54104-3033" device="MOLEX_54104-3033_0_0"/>
<part name="U$1" library="bootcamp" deviceset="FEATHER-1X16" device=""/>
<part name="U$2" library="bootcamp" deviceset="FEATHER-1X12" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J1" gate="G$0" x="10.16" y="35.56" smashed="yes">
<attribute name="NAME" x="10.16" y="33.02" size="2.54" layer="95" align="top-left"/>
</instance>
<instance part="U$1" gate="G$1" x="167.64" y="53.34" smashed="yes">
<attribute name="NAME" x="161.29" y="74.295" size="1.778" layer="95"/>
<attribute name="VALUE" x="161.29" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="-35.56" y="60.96" smashed="yes">
<attribute name="NAME" x="-41.91" y="81.915" size="1.778" layer="95"/>
<attribute name="VALUE" x="-41.91" y="45.72" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
